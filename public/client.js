const openEls = document.querySelectorAll('[data-open]');
const demoRolesArea = document.querySelector('.demo-roles-area');
const loginArea = document.querySelector('.login-area');

function resetForm() {
	if (document.getElementById("ticketForm")) {
		document.getElementById("ticketForm").reset();
	}
}

function sortTable(n, tableId) {
  let table, rows, switching, shouldSwitch, i, x, y, dir, switchcount = 0;

  table = document.getElementById(tableId.toString());
  switching = true;
  dir = 1;

  while (switching) {
  	switching = false;
  	rows = table.rows;
    for (i = 1; i < rows.length - 1; i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName("td")[n];
      y = rows[i + 1].getElementsByTagName("td")[n];

	  if (dir === 1) {
		if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
			shouldSwitch = true;
			break;
		} 
	  } else if (dir === -1) {
	  	if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
			shouldSwitch = true;
			break;
	  	}
	  }
	}
	if (shouldSwitch) {
		rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
		switching = true;
		switchcount++;
	} else if (switchcount === 0 && dir === 1) {
		dir = -1;
		switching = true;
	}
  }
}

// The following function is a WIP
async function deleteTicket(id) {
	const response = await fetch("/tickets/" + id, {
		method: 'DELETE',
		body: JSON.stringify({ title: title }),
		headers: {
			'Content-Type': 'application/json'
		},
		cache: 'no-cache'
	})
	console.log("entered deleteTicket")
	return response.json();
}

async function loginAsViewer() {
	await fetch("/login", {
		method: 'POST',
		body: JSON.stringify({
			username: "tomB@bugwatch.com",
			password: "abc123"
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => {
		window.location.replace("/dashboard");
	}).catch(err => {
		console.log(err);
		return err;
	})
}

async function loginAsContributor() {
	await fetch("/login", {
		method: 'POST',
		body: JSON.stringify({
			username: "mitchmcconnell@bugwatch.com",
			password: "cO3VjwH89:2@vkD"
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => {
		window.location.replace("/dashboard");
	}).catch(err => {
		console.log(err);
		return err;
	})
}

async function loginAsManager() {
	await fetch("/login", {
		method: 'POST',
		body: JSON.stringify({
			username: "brock@bugwatch.com",
			password: "rakuCh4ku"
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => {
		window.location.replace("/dashboard");
	}).catch(err => {
		console.log(err);
		return err;
	})
}

async function loginAsAdmin() {
	await fetch("/login", {
		method: 'POST',
		body: JSON.stringify({
			username: "superwooper@bugwatch.com",
			password: "cu3LnA3G;cwOEt2x@5s"
		}),
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(response => {
		window.location.replace("/dashboard");
	}).catch(err => {
		console.log(err);
		return err;
	})
}

function moveNewTicketButton() {
	if (document.getElementById('navLinks')) {
		const nav = document.querySelector('nav > ul > li');
		const header = document.querySelector('header');
		const leftHeader = document.getElementById('leftHeader');
		const rightHeader = document.getElementById('rightHeader');
		const newTicketButton = document.getElementById('newTicket');
		
	    if (document.documentElement.clientWidth < 555) {
	    	nav.insertBefore(leftHeader, document.querySelector('nav > ul > li > a'))
	    	leftHeader.classList.remove("header-container");
	        header.insertBefore(newTicketButton, rightHeader);
	    } else if (document.documentElement.clientWidth > 555) {
	      	nav.insertBefore(newTicketButton, document.querySelector('nav > ul > li > a'));
	      	leftHeader.classList.add("header-container", "open-sans", "bold-weight")
	      	header.insertBefore(leftHeader, rightHeader);
	    }
	}
}

window.onload = moveNewTicketButton;
window.onresize = moveNewTicketButton;

function toggleNav() {
	const navPane = document.querySelector('nav');

	if (navPane.classList.contains("show-nav")) {
		navPane.classList.remove("show-nav");
	} else {
		navPane.classList.add("show-nav");
	}
}

function showDemoView() {
	demoRolesArea.style.display = "block";
	demoRolesArea.classList.add("is-visible");
	loginArea.style.display = "none";
}

function showLoginForm() {
	loginArea.style.display = "block";
	demoRolesArea.style.display = "none";
}

function clearNotification() {
	if (document.querySelector(".warning-box")) {
		document.querySelector(".warning-box").style.display = 'none';
	} else {
		document.querySelector(".notification-box").style.display = 'none';
	}
}

for (const el of openEls) {
	el.addEventListener("click", function() {
		const modalId = el.dataset.open;
		document.getElementById(modalId).classList.add("is-visible");
		document.querySelector('.modal-content').classList.add("is-visible");
	});
}

document.addEventListener("keyup", e => {
	if (e.key == "Escape" && document.querySelector(".modal.is-visible")) {
		document.querySelector(".modal.is-visible").classList.remove("is-visible");
		document.querySelector('.modal-content').classList.remove("is-visible");
		resetForm();
	}
})

document.addEventListener("click", e => {
	if (e.target == document.querySelector(".modal.is-visible")) {
		document.querySelector(".modal.is-visible").classList.remove("is-visible");
		document.querySelector('.modal-content.is-visible').classList.remove("is-visible");	
	}
})

document.addEventListener("click", e => {
	if (e.target == document.querySelector('[data-close]')) {
		document.querySelector(".modal.is-visible").classList.remove("is-visible");
		document.querySelector('.modal-content.is-visible').classList.remove("is-visible");	
	}
})
