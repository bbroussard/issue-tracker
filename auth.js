const passport = require('passport');
const user = require('./models/user')
const LocalStrategy = require('passport-local').Strategy;

module.exports = (app, passport, users) => {
  app.use(passport.initialize());
  app.use(passport.session());

  passport.use(new LocalStrategy(
    {
      usernameField: 'username',
      passwordField: 'password'
    },
    async function(email, password, done) {
      await users.findOne({ email: email }).then((result) => {
        if (!result || !user.validatePassword(password, result.secret)) {
          return done(null, false, { failureMessage: 'Incorrect credentials submitted. Please try again.'});
        }
        return done(null, result);
      }).catch((err) => {
        return done(err);
      })
    }
  ))

  passport.serializeUser(function(account, done) {
    console.log("serializing user with account " + account.email);
    done(null, account);
  });

  passport.deserializeUser(function(account, done) {
    console.log("deserializing user with account ID " + account._id)
    if (!account) {
      return done(err, null)
    }
    console.log("Login successful for " + account.name)
    done(null, account._id);
  });
}