const user = require('../models/user');
const ticket = require('../models/ticket');
const ObjectId = require('mongodb').ObjectId;

//serves JSON when true
const debugMode = false;

module.exports = (app, db, auth, passport, users, tickets) => {
  app.use(async (req, res, next) => {
    res.locals.typeList = ticket.ticketTypes;
    res.locals.priorityList = ticket.priorityLevels;
    res.locals.ticketList = ticket.getTickets();
    res.locals.userList = await user.getUsers();
    if (req.user) {
      res.locals.currentUser = await user.getUserById(req.user);
      res.locals.currentUserRole = await user.getUserRole(req.user);
    }
    res.locals.cautionMsg = (res.locals.currentUserRole === "viewer" ? "You are unable to create tickets. Please see your system administrator for details." : null)
    next();
  })

  app.use(function(req, res, next) {
    console.log(req.method, req.path + " - " + req.ip);
    next();
  });

  const checkIfAuthenticated = (req, res, next) => {
    console.log("Current user: " + req.user);
    if (req.isAuthenticated && req.user && !debugMode) {
      console.log("user seems to be authenticated...")
      next();
    } else {
      res.render(process.cwd() + '/views/pug/login', { failureMessage: res.message });
    }
  }
  
  app.route('/').get((req, res) => {
    res.redirect('/login')
  })

  app.route('/login').get((req, res) => {
    // prevents notification box from appearing if logged-in user navigates back to login page
    if (req.user) {
      res.render(process.cwd() + '/views/pug/login', {"notificationMessage": null});
    }
    res.render(process.cwd() + '/views/pug/login', {"notificationMessage": req.session.local ? req.session.local.notificationMessage : null});
  }).post( (req, res) => {
    passport.authenticate('local', (err, user) => {
      if (!user) {
        res.render(process.cwd() + '/views/pug/login', {"failureMessage": 'Incorrect credentials submitted. Please try again.'})
        // res.redirect('/login');
      } else {
        req.logIn(user, (err) => {
          if (err) {
            console.log(err);
            return;
          }
          res.redirect('/dashboard');
        })
      }
    }) (req, res);
    // console.log('Logging in...')
    // res.redirect('/dashboard')
    // console.log(`redirecting to ${req.route} from /login`)
  })

  app.get('/dashboard', checkIfAuthenticated, async (req, res) => {
    db.getCollection('tickets').find().toArray().then((items) => {
      res.render(process.cwd() + '/views/pug/dashboard', { 
        "ticketList": items,
        "failureMessage": res.message
      });
    }).catch(err => {
      return err;
    })
  })

  app.route('/tickets').get((req, res) => { 
    db.getCollection('tickets').find().limit(15).toArray().then((items) => {
      if (debugMode) {
        return res.json({ result: items, debugModeMessage: "Debug mode is on. Please toggle debug mode off at line 6 of routes.js." })
      }
      
      user.getUsers().then((result) => {
        res.render(process.cwd() + '/views/pug/ticketList', { 
          "ticketList": items,
          "userList": result
        })
      })
    }).catch(err => {
      res.status(500).json({ err: "Cannot get tickets due to an error: " + err });
      return;
    })
  }).post(checkIfAuthenticated, async (req, res) => {
    const title = req.body.title;
    const creator = await user.getUserById(req.user);
    const type = req.body.type;
    const assignedTo = req.body.assignedTo;
    const description = req.body.description;
    const priority = req.body.priority;
    const status = req.body.status;
    
    tickets.findOne({title: title}).then(result => {
      if (res.locals.currentUserRole === "viewer") {
        console.log("entering condition checking for viewer role in ticket creation")
        res.render(process.cwd() + '/views/pug/dashboard', {"failureMessage": "You are not authorized to create tickets. If this is an error, contact your system administrator."})
        return;
        // res.redirect('/dashboard');
      } else if (result && title !== undefined) {
        res.render(process.cwd() + '/views/pug/dashboard', {"failureMessage": "A ticket already exists with this title."})
        return res.status(403).json({ err: "A ticket already exists with this title." })
      } else if (title === undefined) {
        console.error("This entry is invalid because no name has been entered.");
        return res.status(500).json({ err: 'No name exists'});
      }
      tickets.insertOne(ticket.createTicket(
        title, 
        creator,
        assignedTo,
        type,
        description, 
        priority, 
        ticket.generalStatuses.open,
        req.body.creationDate
        )
      ).then(result => {
        console.log("Post of \"" + title + "\" successful! ---\n", result);
        if (debugMode) {
          res.status(201).json(
            { 
              title,
              creator,
              assignedTo,
              type,
              description,
              priority,
              status
            }
          );
        }
        res.redirect('/tickets/' + ObjectId(result.insertedId));
      }).catch(err => {
        console.error(err);
          return res.status(500).json({ err: err });
      })
    })
  })

  app.route('/tickets/:id').put(checkIfAuthenticated, (req, res) => {
    db.getCollection('tickets').findOneAndUpdate({ _id: req.params.id },
      {
        $currentDate: {
          modifiedDate: true, 
        },
        $set: {
          title: req.body.title,
          type: req.body.type,
          assignee: req.body.assignee,
          description: req.body.description,
          priority: req.body.priority,
          status: req.body.status
        }
      },
      {
        returnOriginal: false,
        ignoreUndefined: true
      }, 
      (err, doc) => {
        if (err) return res.status(500).json({ err: err });
        return res.status(203).json({ tickets: doc });
    })
  }).delete(checkIfAuthenticated, (req, res) => {
    // TODO: set up mechanism for receiving DELETE requests from client
    if (req.body.title && res.locals.currentUserRole === "admin") {
      db.getCollection('tickets').deleteOne({ _id: req.params.id }, (err, result) => {
        if (err) {
          res.status(500).json({ err: err });
          return;
        }
      console.log("Deletion of " + req.body.title + " data successful\n", result);
      return res.status(204).json({ ok: true });
      }) 
    } 
  })

  app.route('/tickets/newest/:sort').get((req, res) => {
    db.getCollection('tickets').find().sort({"creationDate": parseInt(req.params.sort)}).toArray().then(result => {
      res.render(process.cwd() + '/views/pug/ticketList', {"ticketList": result})
    }).catch(err => {
        return {"err": err};
    })
  })

  app.route('/tickets/:id').get((req, res) => {
    tickets.findOne({ _id: ObjectId(req.params.id) }).then(doc => {
      res.render(process.cwd() + '/views/pug/ticketPage', {"ticket": doc})
    }).catch(err => {
      return {"err": "The specified ticket could not be found\n" + err}
    })
  })

  app.route('/tickets/recents').get((req, res) => {

  })

  app.route('/users').post((req, res) => {
    const email = req.body.email;
    const role = req.body.role || user.role.viewer.name;
    const createDateTime = new Date().toISOString();

    users.findOne({ email: email }).then(async (result) => {
      if (result) {
        return res.status(403).json({ err: 'This address is unavailable for registration.' });
      }
      await users.insertOne(
        user.createUser(
          req.body.name,
          email,
          req.body.secret,
          role
        )
      ).then((result) => {
        return res.status(201).json({
          email: email,
          createDateTime: createDateTime,
          role: role
        })
      })
    }).catch(err => {
      return res.status(500).json({ err: err });
    })
  }).get((req, res) => {
    if (req.body.email) {
      user.getUserId(req.body.email).then((result) => {
        return res.status(202).json({ _id: result});
      }).catch((err) => {
        console.error(err);
        return res.status(500).json({ err: err });
      })
    } else {
      users.find().toArray((err, items) => {
        if (err) {
          console.error(err);
          res.status(500).json({ err: err });
          return;
        }
        res.status(200).json({ users: items });
      })
    }
  }).delete((req, res) => {
    users.deleteMany({}, (err, result) => {
      if (err) {
        res.status(500).json({ err: err });
        return;
      }
      console.log("Deletion of 'users' data successful\n", result);
      res.status(204).json({ ok: true });
    })
  })

  app.route('/logout').get((req, res) => {
    req.logout();
    req.session.local = { "notificationMessage": "You have logged out successfully." };
    res.redirect('/login')
  })

  app.use(function (req, res) {
    res.status(404).sendFile(process.cwd() + '/views/not_found.html');
  })
}