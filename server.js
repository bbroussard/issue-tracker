"use strict";

require('custom-env').env('localhost');

const express = require('express');

const db = require('./models/mongoDB');
const user = require('./models/user');
const ticket = require('./models/ticket');
const routes = require('./routes/routes');
const auth = require('./auth');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;

// const ObjectId = require('mongodb').ObjectId;

const app = express();

let tickets, users;
const hourExpiry = new Date(Date.now() + 60 * 60 * 1000)

//only for testing with mocha
module.exports = app;

app.set('view engine', 'pug');
app.use(express.static(__dirname + '/public'));
app.use(session({ 
  secret: process.env.SECRET,
  name: 'sessionID',
  resave: false,
  saveUninitialized: false,
  cookie: {
    sameSite: true,
    httpOnly: true,
  }
}));
app.use(express.urlencoded({
  extended: true
}));
// for parsing credentials sent via demo role buttons
app.use(express.json());

// connect to MongoDB store and start Express server
const port = process.env.PORT || 3000;
const connect = async () => {
  console.log("Connecting to database...");
  await db.connectToDB();
  app.listen(port, () => console.log('Server ready and connected to database'));
}

connect().then(() => {
  tickets = db.cols.tickets;
  users = db.cols.users;
  auth(app, passport, users);
  routes(app, db, auth, passport, users, tickets);
});
