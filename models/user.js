const db = require('./mongoDB');
const bcrypt = require('bcrypt');
const ObjectId = require('mongodb').ObjectId;

function createUser(name, email, secret, role) {
  return {
    name: name,
    email: email,
    secret: scramble(secret),
    role: role 
  }
}

//list of roles and their associated permissions
const role = {
	viewer: { 
    name: "viewer", 
    perms: {
      canView: true,
      canCreateTickets: false,
      canEditTickets: false,
      canComment: false,
      canAttach: false,
      canClose: false,
      canDelete: false,
      canChangeRole: false
    }
  },
  contributor: {
    name: "contributor",
    perms: {
      canView: true,
      canCreateTickets: true,
      canEditTickets: true,
      canComment: true,
      canAttach: true,
      canClose: false,
      canDelete: false,
      canChangeRole: false
    }
  },
	manager: {
    name: "manager",
    perms: {
      canView: true,
      canCreateTickets: true,
      canEditTickets: true,
      canComment: true,
      canAttach: true,
      canClose: true,
      canDelete: true,
      canChangeRole: false
    }
  },
	admin: {
    name: "admin",
    perms: {
      canView: true,
      canCreateTickets: true,
      canEditTickets: true,
      canComment: true,
      canAttach: true,
      canClose: true,
      canDelete: true,
      canChangeRole: true
    }
  }
}

const scramble = (password) => {
  const hash = bcrypt.hashSync(password, 12);
  return hash;
};

const validatePassword = (password, secret) => {
  const compareResult = bcrypt.compareSync(password, secret);
  return compareResult;
}

const getUserId = async (email) => {
  const userDoc = await db.getCollection('users').findOne({ email: email });

  if (userDoc) return userDoc._id
    else return "No user ID exists for that request.";
};

const getUserById = async (id) => {
  return await db.getCollection('users').findOne({ _id: ObjectId(id) }).then(userDoc => {
    if (userDoc) {
      return userDoc.name
    }
    else return "No match found for this ID.";
  });
};

const getUsers = () => {
  return db.getCollection('users').find().toArray()
}

const getUserRole = async (user) => {
  return await db.getCollection('users').findOne({ _id: ObjectId(user) }).then(result => {
    return result.role;
  });
}

module.exports = {
  role,
  createUser,
  scramble,
  validatePassword,
  getUserId,
  getUserById,
  getUserRole,
  getUsers
};