const db = require("./mongoDB");

function createTicket(title, creator, assignee, type, description, priority, status) {
  return {
    title: title,
  	creator: creator,
  	assignee: assignee || "None",
  	type: type,
  	description: description,
  	priority: priority,
  	status: status,
  	creationDate: new Date().toISOString()
  }
}

const ticketTypes = {
  bug: "Bug",
  improvement: "Improvement",
  subtask: "Subtask",
  story: "Story", 
  epic: "Epic", 
  initiative: "Initiative"
}

const priorityLevels = {
  1: "Highest",
  2: "High",
  3: "Medium",
  4: "Low",
  5: "Lowest"
}

const generalStatuses = {
  open: "Open",
  closed: "Closed"
}

const bugStatuses = {
  inProgress: "In Progress",
  fixed: "Fixed",
  willNotFix: "Won't Fix"
}

const subtaskStatuses = {
  started: "Started",
  inProgress: "In Progress",
  complete: "Complete"
}

const storyStatuses = {
  inProgress: "In Progress",
  readyForReview: "Ready for Review",
  complete: "Complete"
}

const epicStatuses = {
  ongoing: "Ongoing",
  suspended: "Suspended",
  complete: "Complete"
}

const findById = (id, callback) => {
  return db.getCollection('tickets').findOne({ _id: id }, callback);
}

const getTickets = () => {
  const ticketList = db.getCollection('tickets').find().toArray();

  if (ticketList) return ticketList
    else return "Could not get list of tickets";
}

module.exports = {
  createTicket,
  ticketTypes,
  priorityLevels,
  generalStatuses,
  bugStatuses,
  subtaskStatuses,
  storyStatuses,
  epicStatuses,
  getTickets,
  findById
}
