const mongo = require('mongodb').MongoClient;
const url = process.env.BT_DATABASE_URL;

let bT;
const cols = {};

const connectToDB = async () => {
  await mongo.connect(url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(client => {
    bT = client.db('bug-tracker');
    cols.tickets = bT.collection('tickets');
    cols.users = bT.collection('users');
  }).catch(err => {
    if (err) {
      throw err;
    } 
  })
}

const getCollection = (collectionId) => { return cols[collectionId] }

const disconnectDB = () => { mongo.close() };

module.exports = {
  connectToDB,
  cols,
  getCollection,
  disconnectDB
};
