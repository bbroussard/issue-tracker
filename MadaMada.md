## MadaMada

A responsive, accessible, light-weight issue tracker whose name means "not yet" or "still more to come" in Japanese. (Anyone who has heard of or played the Blizzard game Overwatch is likely to have heard the character Genji say this as a voice line.)

Why another issue tracker, you ask? MadaMada was born out of frustration with the extremely slow performance of JIRA in looking up and working with stored issues. If you've interacted with a database containing only around 500 of them, you know what I mean.

Also, like all the best inventions, MadaMada was made to excel at one thing: storing and tracking the state of issues in a company.

### Just the facts

**Version**: 0.1.0\
**Client-side**: pure JavaScript (i.e. ES6), possibly with Web Components\
**Middleware**: Express.js, Passport\
**Database**: MongoDB\
**Server-side**: Node.js, Pug

### Installation
1. Create a `".env"` file containing `BT_DATABASE_URL` and `SECRET`. `SECRET` can be a string of random characters, while `BT_DATABASE_URL` must conform to the following: `mongodb://[database username]:[password]@[database host address]:27017` or `mongodb://[database host address]:27017`
1. Save it in the project at the root level.
1. Ensure that you have the latest version of MongoDB installed on your database host.
1. Setup MongoDB on your database host (if running for the first time), then run `mongod --bind_ip [database URI]` via the command line. If the database host is a Windows system, run the preceding via Command Prompt.
1. Install Node.js on the machine that will run the MongoDB instance (if not already installed), then run the "npm start" script present in this project.

### Running after a shutdown
Follow steps 4 and 5 in the [Installation](#installation) section above.

### Known issues

- On smaller screens, the ticket creation dialog's footer may render outside the expected position. As a workaround, utilize a larger screen or increase the size of your browser window.

### Roadmap

The following are planned features and essentials, presented in no particular order within their respective versions:

#### 0.2.0

- Editable tickets

- Ticket linking

- Form validation and sanitization

✔ User accounts and roles with permission sets, along with a demo account that will allow users to preview the app's functionality across roles

#### 0.5.0

- Save ticket as draft

- Color scheme editor, including a pre-designed dark theme

- Upload and associate attachments with tickets

#### 1.0

- Responsive and flexible search (i.e. by affected version, issue type, labels, status)

- Comment system

